/* globals describe, it, expect */
import {readFileSync} from 'fs';
import {ToyRobotDSLListener} from '../src/toy-robot-dsl-listener.class';

describe('Program Syntax', () => {
  it('Good syntax should work', () => {
    const goodSyntax = readFileSync(
      `${__dirname}/fixtures/good-syntax.txt`
    ).toString();
    const dsl = new ToyRobotDSLListener({x: 5, y: 5});
    dsl.execute(goodSyntax);
  });

  it('Bad syntax should throw an error', () => {
    const badSyntax = readFileSync(
      `${__dirname}/fixtures/bad-syntax.txt`
    ).toString();
    const dsl = new ToyRobotDSLListener({x: 5, y: 5});
    expect(() => dsl.execute(badSyntax)).toThrow();
  });
});
