/* globals describe, it, expect, beforeEach */
import {ToyRobotDSLListener} from '../src/toy-robot-dsl-listener.class';

describe('Toy Robot Context', () => {
  let defaultRobot: ToyRobotDSLListener;

  beforeEach(() => {
    defaultRobot = new ToyRobotDSLListener();
  });

  it('Default GridSize should have a 5x5 grid size', () => {
    expect(defaultRobot.GridSize).toStrictEqual({x: 5, y: 5});
  });

  it('Default Position should be undefined', () => {
    expect(defaultRobot.Position).toBe(undefined);
  });

  it('Default Heading to be undefined', () => {
    expect(defaultRobot.Heading).toBe(undefined);
  });

  it('Default HeadingIndex to be undefined', () => {
    expect(defaultRobot.HeadingIndex).toBe(undefined);
  });

  it('GridSize should match the initial grid size', () => {
    const dsl = new ToyRobotDSLListener({x: 6, y: 7});
    expect(dsl.GridSize).toStrictEqual({x: 6, y: 7});
  });

  it('isInBounds() should be true when in bounds', () => {
    expect(defaultRobot.isInBounds({x: 0, y: 0})).toBe(true);
    expect(defaultRobot.isInBounds({x: 4, y: 4})).toBe(true);
  });

  it('isInBounds() should be false when not in bounds', () => {
    expect(defaultRobot.isInBounds({x: 0, y: 5})).toBe(false);
    expect(defaultRobot.isInBounds({x: 5, y: 0})).toBe(false);
    expect(defaultRobot.isInBounds({x: -1, y: 0})).toBe(false);
    expect(defaultRobot.isInBounds({x: 0, y: -1})).toBe(false);
  });
});
