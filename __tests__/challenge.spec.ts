/* globals describe, it, expect, beforeEach */
import {readFileSync} from 'fs';

import {ToyRobotDSLListener} from '../src/toy-robot-dsl-listener.class';

describe('Challenge Examples', () => {
  let defaultRobot: ToyRobotDSLListener;

  beforeEach(() => {
    defaultRobot = new ToyRobotDSLListener({x: 5, y: 5});
  });

  it('Example A should pass', done => {
    const exampleA = readFileSync(
      `${__dirname}/fixtures/example-a.txt`
    ).toString();
    defaultRobot.reporter().subscribe({
      next: report => {
        expect(report).toStrictEqual({
          headingIndex: 0,
          headingName: 'NORTH',
          position: {
            x: 0,
            y: 1,
          },
        });
      },
      error: error => {
        done(error);
      },
      complete: () => {
        done();
      },
    });
    defaultRobot.execute(exampleA);
  });

  it('Example B should pass', done => {
    const exampleB = readFileSync(
      `${__dirname}/fixtures/example-b.txt`
    ).toString();
    defaultRobot.reporter().subscribe({
      next: report => {
        expect(report).toStrictEqual({
          headingIndex: 3,
          headingName: 'WEST',
          position: {
            x: 0,
            y: 0,
          },
        });
      },
      error: error => {
        done(error);
      },
      complete: () => {
        done();
      },
    });
    defaultRobot.execute(exampleB);
  });
});
