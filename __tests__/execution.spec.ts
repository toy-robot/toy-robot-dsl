/* globals describe, it, expect, beforeEach */
import {ToyRobotDSLListener} from '../src/toy-robot-dsl-listener.class';

describe('Toy Robot Execution', () => {
  let defaultRobot: ToyRobotDSLListener;

  beforeEach(() => {
    defaultRobot = new ToyRobotDSLListener();
  });

  describe('PLACE command', () => {
    it('Should be at the correct position after a PLACE command', () => {
      defaultRobot.execute('PLACE 1,2,NORTH');
      expect(defaultRobot.Position).toEqual({x: 1, y: 2});
    });

    it('Should not update the position if a bad position is used in a PLACE command', () => {
      defaultRobot.execute('PLACE 23,2,NORTH');
      expect(defaultRobot.Position).toBe(undefined);
      expect(defaultRobot.HeadingIndex).toBe(undefined);
      expect(defaultRobot.Heading).toBe(undefined);
      defaultRobot.execute('PLACE 2,3,NORTH');
      expect(defaultRobot.Position).toEqual({x: 2, y: 3});
      defaultRobot.execute('PLACE 23,3,NORTH');
      expect(defaultRobot.Position).toEqual({x: 2, y: 3});
    });

    it('Should face the correct direction after a PLACE command', () => {
      defaultRobot.execute('PLACE 2,3,NORTH');
      expect(defaultRobot.Heading).toEqual('NORTH');
      expect(defaultRobot.HeadingIndex).toBe(0);
      defaultRobot.execute('PLACE 2,3,EAST');
      expect(defaultRobot.Heading).toEqual('EAST');
      expect(defaultRobot.HeadingIndex).toBe(1);
      defaultRobot.execute('PLACE 2,3,SOUTH');
      expect(defaultRobot.Heading).toEqual('SOUTH');
      expect(defaultRobot.HeadingIndex).toBe(2);
      defaultRobot.execute('PLACE 2,3,WEST');
      expect(defaultRobot.Heading).toEqual('WEST');
      expect(defaultRobot.HeadingIndex).toBe(3);
    });
  });

  describe('LEFT command', () => {
    it('Should not rotate if it is not placed', () => {
      defaultRobot.execute('LEFT');
      expect(defaultRobot.HeadingIndex).toBe(undefined);
      expect(defaultRobot.Heading).toBe(undefined);
    });

    it('Should face west after facing north', () => {
      defaultRobot.execute('PLACE 0,0,NORTH');
      defaultRobot.execute('LEFT');
      expect(defaultRobot.HeadingIndex).toBe(3);
      expect(defaultRobot.Heading).toBe('WEST');
    });
    it('Should face south after facing west', () => {
      defaultRobot.execute('PLACE 0,0,WEST');
      defaultRobot.execute('LEFT');
      expect(defaultRobot.HeadingIndex).toBe(2);
      expect(defaultRobot.Heading).toBe('SOUTH');
    });
    it('Should face east after facing south', () => {
      defaultRobot.execute('PLACE 0,0,SOUTH');
      defaultRobot.execute('LEFT');
      expect(defaultRobot.HeadingIndex).toBe(1);
      expect(defaultRobot.Heading).toBe('EAST');
    });
    it('Should face north after facing east', () => {
      defaultRobot.execute('PLACE 0,0,EAST');
      defaultRobot.execute('LEFT');
      expect(defaultRobot.HeadingIndex).toBe(0);
      expect(defaultRobot.Heading).toBe('NORTH');
    });
  });

  describe('RIGHT command', () => {
    it('Should not rotate if it is not placed', () => {
      defaultRobot.execute('RIGHT');
      expect(defaultRobot.HeadingIndex).toBe(undefined);
      expect(defaultRobot.Heading).toBe(undefined);
    });

    it('Should face east after facing north', () => {
      defaultRobot.execute('PLACE 0,0,NORTH');
      defaultRobot.execute('RIGHT');
      expect(defaultRobot.HeadingIndex).toBe(1);
      expect(defaultRobot.Heading).toBe('EAST');
    });
    it('Should face south after facing east', () => {
      defaultRobot.execute('PLACE 0,0,EAST');
      defaultRobot.execute('RIGHT');
      expect(defaultRobot.HeadingIndex).toBe(2);
      expect(defaultRobot.Heading).toBe('SOUTH');
    });
    it('Should face west after facing south', () => {
      defaultRobot.execute('PLACE 0,0,SOUTH');
      defaultRobot.execute('RIGHT');
      expect(defaultRobot.HeadingIndex).toBe(3);
      expect(defaultRobot.Heading).toBe('WEST');
    });
    it('Should face north after facing west', () => {
      defaultRobot.execute('PLACE 0,0,WEST');
      defaultRobot.execute('RIGHT');
      expect(defaultRobot.HeadingIndex).toBe(0);
      expect(defaultRobot.Heading).toBe('NORTH');
    });
  });

  describe('MOVE command', () => {
    it('Should increase y position when facing north', () => {
      defaultRobot.execute('PLACE 3,3,NORTH');
      defaultRobot.execute('MOVE');
      expect(defaultRobot.Position).toEqual({x: 3, y: 4});
      expect(defaultRobot.Heading).toBe('NORTH');
    });
    it('Should decrease y position when facing south', () => {
      defaultRobot.execute('PLACE 3,3,SOUTH');
      defaultRobot.execute('MOVE');
      expect(defaultRobot.Position).toEqual({x: 3, y: 2});
      expect(defaultRobot.Heading).toBe('SOUTH');
    });
    it('Should increase x position when facing east', () => {
      defaultRobot.execute('PLACE 3,3,EAST');
      defaultRobot.execute('MOVE');
      expect(defaultRobot.Position).toEqual({x: 4, y: 3});
      expect(defaultRobot.Heading).toBe('EAST');
    });
    it('Should deacrease x position when facing west', () => {
      defaultRobot.execute('PLACE 3,3,WEST');
      defaultRobot.execute('MOVE');
      expect(defaultRobot.Position).toEqual({x: 2, y: 3});
      expect(defaultRobot.Heading).toBe('WEST');
    });
    it('Should not cross northern boundary', () => {
      defaultRobot.execute('PLACE 3,4,NORTH');
      defaultRobot.execute('MOVE');
      expect(defaultRobot.Position).toEqual({x: 3, y: 4});
      expect(defaultRobot.Heading).toBe('NORTH');
    });
    it('Should not cross southern boundary', () => {
      defaultRobot.execute('PLACE 3,0,SOUTH');
      defaultRobot.execute('MOVE');
      expect(defaultRobot.Position).toEqual({x: 3, y: 0});
      expect(defaultRobot.Heading).toBe('SOUTH');
    });
    it('Should not cross eastern boundary', () => {
      defaultRobot.execute('PLACE 4,3,EAST');
      defaultRobot.execute('MOVE');
      expect(defaultRobot.Position).toEqual({x: 4, y: 3});
      expect(defaultRobot.Heading).toBe('EAST');
    });
    it('Should not cross western boundary', () => {
      defaultRobot.execute('PLACE 0,3,WEST');
      defaultRobot.execute('MOVE');
      expect(defaultRobot.Position).toEqual({x: 0, y: 3});
      expect(defaultRobot.Heading).toBe('WEST');
    });
  });
  describe('REPORT command', () => {
    it('Should report undefined for an unplaced robot', done => {
      defaultRobot.reporter().subscribe({
        next: report => {
          expect(report).toStrictEqual({
            headingIndex: undefined,
            headingName: undefined,
            position: undefined,
          });
        },
        error: error => {
          done(error);
        },
        complete: () => {
          done();
        },
      });
      defaultRobot.execute('REPORT');
    });
    it('Should report position and heading for a placed robot', done => {
      defaultRobot.reporter().subscribe({
        next: report => {
          expect(report).toEqual({
            headingIndex: 0,
            headingName: 'NORTH',
            position: {
              x: 2,
              y: 3,
            },
          });
        },
        error: error => {
          done(error);
        },
        complete: () => {
          done();
        },
      });
      defaultRobot.execute('PLACE 2,3, NORTH');
      defaultRobot.execute('REPORT');
    });
  });
});
