#!/usr/bin/env node

const { exit } = require('process');
const { ToyRobot } = require('./build/src/index');
const { readFileSync } = require('fs');

if (process.argv.length !== 3) {
    console.log(`USAGE: ${process.argv[1]} <toy robot file>`);
    exit(1);
}

const toyRobotProgram = readFileSync(process.argv[2]).toString();

const robot = new ToyRobot();

robot.reporter().subscribe({
    next:(report) => {
        console.log(`${report.position.x},${report.position.y},${report.headingName}`);
    },
    error:(error) => {
        console.error(`ERROR`, error);
    }
});

robot.execute(toyRobotProgram);