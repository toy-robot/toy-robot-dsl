# ToyrRobot DSL

This is the main part of the MrYum Toy Robot Challenge.

This module provides the DSL for the Toy Robot language and also contains a
CLI to allow basic testing of the DSL.

Although the challenge should be simple enough to solve using an map of regex to functions,
it neither robust robust nor sustainable. Instead, let's do things the right way and
build out a grammar.

## Quickstart
Tested on WSL only, should work on Mac and other *nix systems.

1. Clone the repository
2. Enter the directory
3. Install dependencies
4. Run the CLI and specify the toy robot program file.

```
git clone https://gitlab.com/toy-robot/toy-robot-dsl.git
cd toy-robot-dsl
npm i
./toyrobotcli.js ./__tests__/fixtures/example-a.txt
./toyrobotcli.js ./__tests__/fixtures/example-b.txt
```

Sorry, the CLI is very quick and dirty!

I've included all the `./build` files in the repo so you don't have to install the antlr4 dependency and build it yourself. It should just work out of the box after cloning (assuming node is installed).

## Getting Started
The first things to do (aside from this README) is to setup the base of the project with
initial dependencies. 

In this case I will be using `npx gts` to initialise a basic `typescript` project that uses
some Google defaults (gts stands for Google TypeScript). I'll clear out the sample project and replace it with an empty (yet valid!) file.

**NOTE:** This project will be commiting the `build` folder since the reviewers may not wish to
install the java grammar compiler (more on this later).

## The Grammar
I'll use `antlr4` which can take in a grammar and create all the base components such as tokenize,
lexer and parser. Antlr4 can output to a bunch of languages, in this case we will use typescript.

The javascript support is quite robust, but the typescript is still alpha and has a few little
things we'll need to take masage to make it play nice.

To build this project you will need [antlr4 installed](https://www.antlr.org/index.html) and available on your command line. 

Please note this project was created using WSL so *should* work fine on Mac and *nix, but it has
not been tested on Windows.

I'll start by defining all the tokens and rules in our grammar and we'll built out 
and refine from there.

## Building the Grammar
Now we've got the base of the grammar we can build the parser, lexer, tokenizer etc.

```
npm run antlr4ts
```

This will output to `./src/grammar`. This will need to be regenerated anytime the grammar changes.

The generated files have been exlcuded from lint checking, and they have some things that
will generally cause warnings. You can stop the IDE from showing these errors by adding to
the files `toyrobotParser.ts` and `toyrobotLexer.ts` the following comment to the top of the files:

```
// @ts-nocheck
```

This is a known bug in `antlr4ts` and rather annoying as you need to add it each time you 
regenerate these files, or just try and ignore the glaring red files in your IDE!

## Configure Testing with Jest
Before moving ahead we'll get some tests going with jest, or ts-jest since we are
using typescript.

```
npm test
```

## State Machine
Normally I would look at using something like `xstate.js` for state machine tracking, but to save
time I'm just going for a 'quick and dirty' method by tracking inside a class. A more
functional approach would be nicer.

## Movement
I'll track position and movement using a 2D transform (x, y). The heading will be tracked
as a simple map of NORTH, EAST, SOUTH, WEST. These will be used in such a way that they 
iterate clockwise (NORTH -> EAST -> SOUTH -> WEST (-> NORTH)) in a modulo fashion.

## Implementing the Grammar
Time to start implementing the grammar. There are two patterns to do it: 
using a visitor or a listener.

I've decided to go with the listener pattern.

Looking at the listener interface we see only 3 useful listeners. While these
can be used to implement the grammar, we can do better, so time to revisit the 
grammar file and be more specific about the parts we care about.

With some more specifics on the grammar we have more methods that allow us
to better model the state based on the language.

## Reporting
The REPORT command reports through an RxJs Observable. Before executing the program, the
caller can subscribe to `report()`. The report will take the form:
```
interface ToyRobotReport {
  position: Transform | undefined;
  headingIndex: number | undefined;
  headingName: string | undefined;
}
```
