grammar toyrobot;

robotcommands: NEWLINE* robotcommand* EOF ;

robotcommand : ( placement | movement | report ) NEWLINE*;

placement: PLACE x=NUMBER ',' y=NUMBER ',' d=DIRECTION ;


movement : MOVE    #MoveForward
         | LEFT    #RotateLeft
         | RIGHT   #RotateRight
         ;

report: REPORT ;

PLACE:  'PLACE' ;
DIRECTION: ('NORTH' | 'SOUTH' | 'EAST' | 'WEST') ;

MOVE:   'MOVE' ;
LEFT:   'LEFT' ;
RIGHT:  'RIGHT' ;

REPORT: 'REPORT' ;

fragment DIGIT : [0-9] ;

NUMBER: DIGIT+;

// The WHITESPACE rule allows us to skip over whitespace and not consider it in the grammar
WHITESPACE: (' '|'\t')+ -> skip;
NEWLINE : ('\r'? '\n' | '\n')+;


