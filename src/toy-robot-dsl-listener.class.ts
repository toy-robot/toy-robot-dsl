import {toyrobotListener} from './grammar/toyrobotListener';
import {
  MoveForwardContext,
  PlacementContext,
  ReportContext,
  RotateLeftContext,
  RotateRightContext,
  toyrobotParser,
} from './grammar/toyrobotParser';

import DebugFactory = require('debug');
import {HeadingIndex, HeadingMap} from './heading.constants';
import {Transform} from './transform.interface';
import {CharStreams} from 'antlr4ts/CharStreams';
import {CommonTokenStream} from 'antlr4ts/CommonTokenStream';
import {ParseTreeListener} from 'antlr4ts/tree/ParseTreeListener';
import {toyrobotLexer} from './grammar/toyrobotLexer';
import {ParseTreeWalker} from 'antlr4ts/tree/ParseTreeWalker';
import {SyntaxErrorListener} from './syntax-error-listener.class';
import {Observable, Subscriber} from 'rxjs';
import {ToyRobotReport} from './toy-robot-report.interface';

const debug = DebugFactory('toyrobot:dsl');

/**
 * The Toy Robot DSL Listener processes the toy robot program.
 */
export class ToyRobotDSLListener implements toyrobotListener {
  /**
   * The position of the robot or undefined if it hasn't yet been placed.
   */
  protected position?: Transform;

  /**
   * The heading index of the robot or undefined if it hasn't yet been placed.
   */
  protected heading?: number;

  protected subscriber?: Subscriber<unknown>;

  /**
   * Initialise the context of the toy robot.
   * @param gridSize Initialised the grid size. Default is 5x5
   */
  constructor(protected gridSize: Transform = {x: 5, y: 5}) {}

  /**
   * Get the current heading as a direction, i.e. NORTH, SOUTH, EAST, WEST
   */
  get Heading(): string | undefined {
    if (this.heading === undefined) {
      debug('[Heading] Valid robot placement is yet to been made.');
      return undefined;
    }
    const theHeading = HeadingIndex[this.heading];
    debug(`[Heading] ${theHeading} (${this.heading})`);
    return theHeading;
  }

  /**
   * Get the current heading as an index of possible directions
   */
  get HeadingIndex(): number | undefined {
    return this.heading;
  }

  /**
   * Get the current position
   */
  get Position(): Transform | undefined {
    return this.position;
  }

  /**
   * Get the current size of the grid
   */
  get GridSize(): Transform {
    return this.gridSize;
  }

  /**
   * Tests if a position is within the bounds of the grid
   * @param position A position transform to test
   */
  isInBounds(position: Transform): boolean {
    const isBounded =
      position.x >= 0 &&
      position.y >= 0 &&
      position.x < this.gridSize.x &&
      position.y < this.gridSize.y;
    return isBounded;
  }

  /**
   * Executes the program for this toy robot's context
   * @param program The program to execute
   */
  execute(program: string) {
    const syntaxErrorListener = new SyntaxErrorListener();
    const inputStream = CharStreams.fromString(program);
    const lexer = new toyrobotLexer(inputStream);
    lexer.removeErrorListeners();
    lexer.addErrorListener(syntaxErrorListener);
    const tokenStream = new CommonTokenStream(lexer);
    const parser = new toyrobotParser(tokenStream);
    parser.removeErrorListeners();
    parser.addErrorListener(syntaxErrorListener);

    const tree = parser.robotcommands();
    ParseTreeWalker.DEFAULT.walk(this as ParseTreeListener, tree);
    return tree;
  }

  exitRobotcommands() {
    this.subscriber?.complete();
  }

  enterPlacement(ctx: PlacementContext) {
    // Although these next 3 ctx properties are optional, they will always be set because of the structure of our grammar
    // Further, they will always be of the right type, the grammar rules act as a validator
    const x: number = +ctx._x.text!;
    const y: number = +ctx._y.text!;
    const headingName: string = ctx._d.text!;
    const heading: number = HeadingIndex.indexOf(headingName);

    const position: Transform = {x, y};
    if (this.isInBounds(position)) {
      debug(`[PLACE] x:${x}, y: ${y}, heading: ${headingName} (${heading})}`);
      this.position = position;
      this.heading = heading;
    }
  }

  /**
   * Create an rxjs observer that can be subscribed to for monitoring
   * output from the REPORT command.
   */
  reporter() {
    return new Observable<ToyRobotReport>(subscriber => {
      this.subscriber = subscriber;
    });
  }

  enterReport(ctx: ReportContext) {
    const report: ToyRobotReport = {
      headingIndex: this.HeadingIndex ?? undefined,
      headingName: this.Heading,
      position: this.Position,
    };
    debug(`[REPORT] Report: ${JSON.stringify(report)}`);
    this.subscriber?.next(report);
  }

  enterMoveForward(ctx: MoveForwardContext) {
    if (!this.position) {
      debug('[MOVE] Robot can not move, it needs to be positioned first.');
      return;
    }
    const heading = HeadingIndex[this.heading!];
    const movementTransform = HeadingMap.get(heading);
    const nextPosition: Transform = {
      x: this.position.x + movementTransform!.x,
      y: this.position.y + movementTransform!.y,
    };
    if (!this.isInBounds(nextPosition)) {
      debug(
        `[MOVE] Can not move off grid: ${this.position} => ${nextPosition}, ${this.Heading} (${this.heading}) ]`
      );
      return;
    }
    this.position = nextPosition;
    debug(
      `[MOVE] New position: ${this.position}, ${this.Heading} (${this.heading}) ]`
    );
  }

  enterRotateLeft(ctx: RotateLeftContext) {
    if (!this.position) {
      debug('[LEFT] Robot can not rotate, it needs to be positioned first.');
      return;
    }
    this.heading! -= this.heading === 0 ? -(HeadingIndex.length - 1) : 1;
    debug(`[LEFT] Now facing ${this.Heading} (index: ${this.heading})`);
  }

  enterRotateRight(ctx: RotateRightContext) {
    if (!this.position) {
      debug('[RIGHT] Robot can not rotate, it needs to be positioned first.');
      return;
    }
    this.heading! = (this.heading! + 1) % HeadingIndex.length;
    debug(`[LEFT] Now facing ${this.Heading} (index: ${this.heading})`);
  }
}
