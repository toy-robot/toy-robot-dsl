/**
 * Representation of 2D transform in the x and y axis.
 */
export interface Transform {
  /**
   * Transform in the x axis
   */
  x: number;
  /**
   * Transform in the y axis
   */
  y: number;
}
