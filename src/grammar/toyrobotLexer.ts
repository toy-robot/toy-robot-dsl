// Generated from grammar/toyrobot.g4 by ANTLR 4.7.3-SNAPSHOT
// @ts-nocheck

import { ATN } from "antlr4ts/atn/ATN";
import { ATNDeserializer } from "antlr4ts/atn/ATNDeserializer";
import { CharStream } from "antlr4ts/CharStream";
import { Lexer } from "antlr4ts/Lexer";
import { LexerATNSimulator } from "antlr4ts/atn/LexerATNSimulator";
import { NotNull } from "antlr4ts/Decorators";
import { Override } from "antlr4ts/Decorators";
import { RuleContext } from "antlr4ts/RuleContext";
import { Vocabulary } from "antlr4ts/Vocabulary";
import { VocabularyImpl } from "antlr4ts/VocabularyImpl";

import * as Utils from "antlr4ts/misc/Utils";


export class toyrobotLexer extends Lexer {
	public static readonly T__0 = 1;
	public static readonly PLACE = 2;
	public static readonly DIRECTION = 3;
	public static readonly MOVE = 4;
	public static readonly LEFT = 5;
	public static readonly RIGHT = 6;
	public static readonly REPORT = 7;
	public static readonly NUMBER = 8;
	public static readonly WHITESPACE = 9;
	public static readonly NEWLINE = 10;

	// tslint:disable:no-trailing-whitespace
	public static readonly channelNames: string[] = [
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN",
	];

	// tslint:disable:no-trailing-whitespace
	public static readonly modeNames: string[] = [
		"DEFAULT_MODE",
	];

	public static readonly ruleNames: string[] = [
		"T__0", "PLACE", "DIRECTION", "MOVE", "LEFT", "RIGHT", "REPORT", "DIGIT", 
		"NUMBER", "WHITESPACE", "NEWLINE",
	];

	private static readonly _LITERAL_NAMES: Array<string | undefined> = [
		undefined, "','", "'PLACE'", undefined, "'MOVE'", "'LEFT'", "'RIGHT'", 
		"'REPORT'",
	];
	private static readonly _SYMBOLIC_NAMES: Array<string | undefined> = [
		undefined, undefined, "PLACE", "DIRECTION", "MOVE", "LEFT", "RIGHT", "REPORT", 
		"NUMBER", "WHITESPACE", "NEWLINE",
	];
	public static readonly VOCABULARY: Vocabulary = new VocabularyImpl(toyrobotLexer._LITERAL_NAMES, toyrobotLexer._SYMBOLIC_NAMES, []);

	// @Override
	// @NotNull
	public get vocabulary(): Vocabulary {
		return toyrobotLexer.VOCABULARY;
	}
	// tslint:enable:no-trailing-whitespace


	constructor(input: CharStream) {
		super(input);
		this._interp = new LexerATNSimulator(toyrobotLexer._ATN, this);
	}

	// @Override
	public get grammarFileName(): string { return "toyrobot.g4"; }

	// @Override
	public get ruleNames(): string[] { return toyrobotLexer.ruleNames; }

	// @Override
	public get serializedATN(): string { return toyrobotLexer._serializedATN; }

	// @Override
	public get channelNames(): string[] { return toyrobotLexer.channelNames; }

	// @Override
	public get modeNames(): string[] { return toyrobotLexer.modeNames; }

	public static readonly _serializedATN: string =
		"\x03\uC91D\uCABA\u058D\uAFBA\u4F53\u0607\uEA8B\uC241\x02\fc\b\x01\x04" +
		"\x02\t\x02\x04\x03\t\x03\x04\x04\t\x04\x04\x05\t\x05\x04\x06\t\x06\x04" +
		"\x07\t\x07\x04\b\t\b\x04\t\t\t\x04\n\t\n\x04\v\t\v\x04\f\t\f\x03\x02\x03" +
		"\x02\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x04\x03\x04\x03" +
		"\x04\x03\x04\x03\x04\x03\x04\x03\x04\x03\x04\x03\x04\x03\x04\x03\x04\x03" +
		"\x04\x03\x04\x03\x04\x03\x04\x03\x04\x03\x04\x03\x04\x05\x044\n\x04\x03" +
		"\x05\x03\x05\x03\x05\x03\x05\x03\x05\x03\x06\x03\x06\x03\x06\x03\x06\x03" +
		"\x06\x03\x07\x03\x07\x03\x07\x03\x07\x03\x07\x03\x07\x03\b\x03\b\x03\b" +
		"\x03\b\x03\b\x03\b\x03\b\x03\t\x03\t\x03\n\x06\nP\n\n\r\n\x0E\nQ\x03\v" +
		"\x06\vU\n\v\r\v\x0E\vV\x03\v\x03\v\x03\f\x05\f\\\n\f\x03\f\x03\f\x06\f" +
		"`\n\f\r\f\x0E\fa\x02\x02\x02\r\x03\x02\x03\x05\x02\x04\x07\x02\x05\t\x02" +
		"\x06\v\x02\x07\r\x02\b\x0F\x02\t\x11\x02\x02\x13\x02\n\x15\x02\v\x17\x02" +
		"\f\x03\x02\x04\x03\x022;\x04\x02\v\v\"\"\x02i\x02\x03\x03\x02\x02\x02" +
		"\x02\x05\x03\x02\x02\x02\x02\x07\x03\x02\x02\x02\x02\t\x03\x02\x02\x02" +
		"\x02\v\x03\x02\x02\x02\x02\r\x03\x02\x02\x02\x02\x0F\x03\x02\x02\x02\x02" +
		"\x13\x03\x02\x02\x02\x02\x15\x03\x02\x02\x02\x02\x17\x03\x02\x02\x02\x03" +
		"\x19\x03\x02\x02\x02\x05\x1B\x03\x02\x02\x02\x073\x03\x02\x02\x02\t5\x03" +
		"\x02\x02\x02\v:\x03\x02\x02\x02\r?\x03\x02\x02\x02\x0FE\x03\x02\x02\x02" +
		"\x11L\x03\x02\x02\x02\x13O\x03\x02\x02\x02\x15T\x03\x02\x02\x02\x17_\x03" +
		"\x02\x02\x02\x19\x1A\x07.\x02\x02\x1A\x04\x03\x02\x02\x02\x1B\x1C\x07" +
		"R\x02\x02\x1C\x1D\x07N\x02\x02\x1D\x1E\x07C\x02\x02\x1E\x1F\x07E\x02\x02" +
		"\x1F \x07G\x02\x02 \x06\x03\x02\x02\x02!\"\x07P\x02\x02\"#\x07Q\x02\x02" +
		"#$\x07T\x02\x02$%\x07V\x02\x02%4\x07J\x02\x02&\'\x07U\x02\x02\'(\x07Q" +
		"\x02\x02()\x07W\x02\x02)*\x07V\x02\x02*4\x07J\x02\x02+,\x07G\x02\x02," +
		"-\x07C\x02\x02-.\x07U\x02\x02.4\x07V\x02\x02/0\x07Y\x02\x0201\x07G\x02" +
		"\x0212\x07U\x02\x0224\x07V\x02\x023!\x03\x02\x02\x023&\x03\x02\x02\x02" +
		"3+\x03\x02\x02\x023/\x03\x02\x02\x024\b\x03\x02\x02\x0256\x07O\x02\x02" +
		"67\x07Q\x02\x0278\x07X\x02\x0289\x07G\x02\x029\n\x03\x02\x02\x02:;\x07" +
		"N\x02\x02;<\x07G\x02\x02<=\x07H\x02\x02=>\x07V\x02\x02>\f\x03\x02\x02" +
		"\x02?@\x07T\x02\x02@A\x07K\x02\x02AB\x07I\x02\x02BC\x07J\x02\x02CD\x07" +
		"V\x02\x02D\x0E\x03\x02\x02\x02EF\x07T\x02\x02FG\x07G\x02\x02GH\x07R\x02" +
		"\x02HI\x07Q\x02\x02IJ\x07T\x02\x02JK\x07V\x02\x02K\x10\x03\x02\x02\x02" +
		"LM\t\x02\x02\x02M\x12\x03\x02\x02\x02NP\x05\x11\t\x02ON\x03\x02\x02\x02" +
		"PQ\x03\x02\x02\x02QO\x03\x02\x02\x02QR\x03\x02\x02\x02R\x14\x03\x02\x02" +
		"\x02SU\t\x03\x02\x02TS\x03\x02\x02\x02UV\x03\x02\x02\x02VT\x03\x02\x02" +
		"\x02VW\x03\x02\x02\x02WX\x03\x02\x02\x02XY\b\v\x02\x02Y\x16\x03\x02\x02" +
		"\x02Z\\\x07\x0F\x02\x02[Z\x03\x02\x02\x02[\\\x03\x02\x02\x02\\]\x03\x02" +
		"\x02\x02]`\x07\f\x02\x02^`\x07\f\x02\x02_[\x03\x02\x02\x02_^\x03\x02\x02" +
		"\x02`a\x03\x02\x02\x02a_\x03\x02\x02\x02ab\x03\x02\x02\x02b\x18\x03\x02" +
		"\x02\x02\t\x023QV[_a\x03\b\x02\x02";
	public static __ATN: ATN;
	public static get _ATN(): ATN {
		if (!toyrobotLexer.__ATN) {
			toyrobotLexer.__ATN = new ATNDeserializer().deserialize(Utils.toCharArray(toyrobotLexer._serializedATN));
		}

		return toyrobotLexer.__ATN;
	}

}

