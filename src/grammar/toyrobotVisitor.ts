// Generated from grammar/toyrobot.g4 by ANTLR 4.7.3-SNAPSHOT


import { ParseTreeVisitor } from "antlr4ts/tree/ParseTreeVisitor";

import { MoveForwardContext } from "./toyrobotParser";
import { RotateLeftContext } from "./toyrobotParser";
import { RotateRightContext } from "./toyrobotParser";
import { RobotcommandsContext } from "./toyrobotParser";
import { RobotcommandContext } from "./toyrobotParser";
import { PlacementContext } from "./toyrobotParser";
import { MovementContext } from "./toyrobotParser";
import { ReportContext } from "./toyrobotParser";


/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by `toyrobotParser`.
 *
 * @param <Result> The return type of the visit operation. Use `void` for
 * operations with no return type.
 */
export interface toyrobotVisitor<Result> extends ParseTreeVisitor<Result> {
	/**
	 * Visit a parse tree produced by the `MoveForward`
	 * labeled alternative in `toyrobotParser.movement`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitMoveForward?: (ctx: MoveForwardContext) => Result;

	/**
	 * Visit a parse tree produced by the `RotateLeft`
	 * labeled alternative in `toyrobotParser.movement`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitRotateLeft?: (ctx: RotateLeftContext) => Result;

	/**
	 * Visit a parse tree produced by the `RotateRight`
	 * labeled alternative in `toyrobotParser.movement`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitRotateRight?: (ctx: RotateRightContext) => Result;

	/**
	 * Visit a parse tree produced by `toyrobotParser.robotcommands`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitRobotcommands?: (ctx: RobotcommandsContext) => Result;

	/**
	 * Visit a parse tree produced by `toyrobotParser.robotcommand`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitRobotcommand?: (ctx: RobotcommandContext) => Result;

	/**
	 * Visit a parse tree produced by `toyrobotParser.placement`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitPlacement?: (ctx: PlacementContext) => Result;

	/**
	 * Visit a parse tree produced by `toyrobotParser.movement`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitMovement?: (ctx: MovementContext) => Result;

	/**
	 * Visit a parse tree produced by `toyrobotParser.report`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitReport?: (ctx: ReportContext) => Result;
}

