import {Transform} from './transform.interface';

/**
 *  Define headings starting at NORTH and going clockwise
 * */
export const HeadingIndex = ['NORTH', 'EAST', 'SOUTH', 'WEST'];

/**
 * Keep movement translations as a simple matrix, no need for fancy arithmetic
 */
export const HeadingMap = new Map<string, Transform>([
  ['NORTH', {x: 0, y: 1}], // To move north the y axis increases
  ['EAST', {x: 1, y: 0}], // To move east the x axis increases
  ['SOUTH', {x: 0, y: -1}], // To move south the y axis decreases
  ['WEST', {x: -1, y: 0}], // To move west the x axis dencreases
]);
