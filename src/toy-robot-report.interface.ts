import {Transform} from './transform.interface';

/**
 * Report provided by the toy robot in response to the REPORT command.
 */
export interface ToyRobotReport {
  position: Transform | undefined;
  headingIndex: number | undefined;
  headingName: string | undefined;
}
