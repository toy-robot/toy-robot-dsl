import {ANTLRErrorListener} from 'antlr4ts';

/**
 * A syntax error listener for our toy robot.
 */
export class SyntaxErrorListener implements ANTLRErrorListener<unknown> {
  syntaxError(
    _recogniser: unknown,
    _token: unknown,
    line: number,
    column: number,
    message: string
    // _e: unknown
  ) {
    const errorMessage = `${line}:${column + 1} ${message}`;
    const error = new Error(errorMessage);
    throw error;
  }
}
