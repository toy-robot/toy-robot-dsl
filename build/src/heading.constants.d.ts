import { Transform } from './transform.interface';
/**
 *  Define headings starting at NORTH and going clockwise
 * */
export declare const HeadingIndex: string[];
/**
 * Keep movement translations as a simple matrix, no need for fancy arithmetic
 */
export declare const HeadingMap: Map<string, Transform>;
