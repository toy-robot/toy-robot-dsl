"use strict";
// Generated from grammar/toyrobot.g4 by ANTLR 4.7.3-SNAPSHOT
// @ts-nocheck
Object.defineProperty(exports, "__esModule", { value: true });
const ATNDeserializer_1 = require("antlr4ts/atn/ATNDeserializer");
const NoViableAltException_1 = require("antlr4ts/NoViableAltException");
const Parser_1 = require("antlr4ts/Parser");
const ParserRuleContext_1 = require("antlr4ts/ParserRuleContext");
const ParserATNSimulator_1 = require("antlr4ts/atn/ParserATNSimulator");
const RecognitionException_1 = require("antlr4ts/RecognitionException");
const VocabularyImpl_1 = require("antlr4ts/VocabularyImpl");
const Utils = require("antlr4ts/misc/Utils");
class toyrobotParser extends Parser_1.Parser {
    constructor(input) {
        super(input);
        this._interp = new ParserATNSimulator_1.ParserATNSimulator(toyrobotParser._ATN, this);
    }
    // @Override
    // @NotNull
    get vocabulary() {
        return toyrobotParser.VOCABULARY;
    }
    // tslint:enable:no-trailing-whitespace
    // @Override
    get grammarFileName() { return "toyrobot.g4"; }
    // @Override
    get ruleNames() { return toyrobotParser.ruleNames; }
    // @Override
    get serializedATN() { return toyrobotParser._serializedATN; }
    // @RuleVersion(0)
    robotcommands() {
        let _localctx = new RobotcommandsContext(this._ctx, this.state);
        this.enterRule(_localctx, 0, toyrobotParser.RULE_robotcommands);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 13;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === toyrobotParser.NEWLINE) {
                    {
                        {
                            this.state = 10;
                            this.match(toyrobotParser.NEWLINE);
                        }
                    }
                    this.state = 15;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
                this.state = 19;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while ((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << toyrobotParser.PLACE) | (1 << toyrobotParser.MOVE) | (1 << toyrobotParser.LEFT) | (1 << toyrobotParser.RIGHT) | (1 << toyrobotParser.REPORT))) !== 0)) {
                    {
                        {
                            this.state = 16;
                            this.robotcommand();
                        }
                    }
                    this.state = 21;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
                this.state = 22;
                this.match(toyrobotParser.EOF);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    robotcommand() {
        let _localctx = new RobotcommandContext(this._ctx, this.state);
        this.enterRule(_localctx, 2, toyrobotParser.RULE_robotcommand);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 27;
                this._errHandler.sync(this);
                switch (this._input.LA(1)) {
                    case toyrobotParser.PLACE:
                        {
                            this.state = 24;
                            this.placement();
                        }
                        break;
                    case toyrobotParser.MOVE:
                    case toyrobotParser.LEFT:
                    case toyrobotParser.RIGHT:
                        {
                            this.state = 25;
                            this.movement();
                        }
                        break;
                    case toyrobotParser.REPORT:
                        {
                            this.state = 26;
                            this.report();
                        }
                        break;
                    default:
                        throw new NoViableAltException_1.NoViableAltException(this);
                }
                this.state = 32;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === toyrobotParser.NEWLINE) {
                    {
                        {
                            this.state = 29;
                            this.match(toyrobotParser.NEWLINE);
                        }
                    }
                    this.state = 34;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    placement() {
        let _localctx = new PlacementContext(this._ctx, this.state);
        this.enterRule(_localctx, 4, toyrobotParser.RULE_placement);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 35;
                this.match(toyrobotParser.PLACE);
                this.state = 36;
                _localctx._x = this.match(toyrobotParser.NUMBER);
                this.state = 37;
                this.match(toyrobotParser.T__0);
                this.state = 38;
                _localctx._y = this.match(toyrobotParser.NUMBER);
                this.state = 39;
                this.match(toyrobotParser.T__0);
                this.state = 40;
                _localctx._d = this.match(toyrobotParser.DIRECTION);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    movement() {
        let _localctx = new MovementContext(this._ctx, this.state);
        this.enterRule(_localctx, 6, toyrobotParser.RULE_movement);
        try {
            this.state = 45;
            this._errHandler.sync(this);
            switch (this._input.LA(1)) {
                case toyrobotParser.MOVE:
                    _localctx = new MoveForwardContext(_localctx);
                    this.enterOuterAlt(_localctx, 1);
                    {
                        this.state = 42;
                        this.match(toyrobotParser.MOVE);
                    }
                    break;
                case toyrobotParser.LEFT:
                    _localctx = new RotateLeftContext(_localctx);
                    this.enterOuterAlt(_localctx, 2);
                    {
                        this.state = 43;
                        this.match(toyrobotParser.LEFT);
                    }
                    break;
                case toyrobotParser.RIGHT:
                    _localctx = new RotateRightContext(_localctx);
                    this.enterOuterAlt(_localctx, 3);
                    {
                        this.state = 44;
                        this.match(toyrobotParser.RIGHT);
                    }
                    break;
                default:
                    throw new NoViableAltException_1.NoViableAltException(this);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    report() {
        let _localctx = new ReportContext(this._ctx, this.state);
        this.enterRule(_localctx, 8, toyrobotParser.RULE_report);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 47;
                this.match(toyrobotParser.REPORT);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    static get _ATN() {
        if (!toyrobotParser.__ATN) {
            toyrobotParser.__ATN = new ATNDeserializer_1.ATNDeserializer().deserialize(Utils.toCharArray(toyrobotParser._serializedATN));
        }
        return toyrobotParser.__ATN;
    }
}
exports.toyrobotParser = toyrobotParser;
toyrobotParser.T__0 = 1;
toyrobotParser.PLACE = 2;
toyrobotParser.DIRECTION = 3;
toyrobotParser.MOVE = 4;
toyrobotParser.LEFT = 5;
toyrobotParser.RIGHT = 6;
toyrobotParser.REPORT = 7;
toyrobotParser.NUMBER = 8;
toyrobotParser.WHITESPACE = 9;
toyrobotParser.NEWLINE = 10;
toyrobotParser.RULE_robotcommands = 0;
toyrobotParser.RULE_robotcommand = 1;
toyrobotParser.RULE_placement = 2;
toyrobotParser.RULE_movement = 3;
toyrobotParser.RULE_report = 4;
// tslint:disable:no-trailing-whitespace
toyrobotParser.ruleNames = [
    "robotcommands", "robotcommand", "placement", "movement", "report",
];
toyrobotParser._LITERAL_NAMES = [
    undefined, "','", "'PLACE'", undefined, "'MOVE'", "'LEFT'", "'RIGHT'",
    "'REPORT'",
];
toyrobotParser._SYMBOLIC_NAMES = [
    undefined, undefined, "PLACE", "DIRECTION", "MOVE", "LEFT", "RIGHT", "REPORT",
    "NUMBER", "WHITESPACE", "NEWLINE",
];
toyrobotParser.VOCABULARY = new VocabularyImpl_1.VocabularyImpl(toyrobotParser._LITERAL_NAMES, toyrobotParser._SYMBOLIC_NAMES, []);
toyrobotParser._serializedATN = "\x03\uC91D\uCABA\u058D\uAFBA\u4F53\u0607\uEA8B\uC241\x03\f4\x04\x02\t" +
    "\x02\x04\x03\t\x03\x04\x04\t\x04\x04\x05\t\x05\x04\x06\t\x06\x03\x02\x07" +
    "\x02\x0E\n\x02\f\x02\x0E\x02\x11\v\x02\x03\x02\x07\x02\x14\n\x02\f\x02" +
    "\x0E\x02\x17\v\x02\x03\x02\x03\x02\x03\x03\x03\x03\x03\x03\x05\x03\x1E" +
    "\n\x03\x03\x03\x07\x03!\n\x03\f\x03\x0E\x03$\v\x03\x03\x04\x03\x04\x03" +
    "\x04\x03\x04\x03\x04\x03\x04\x03\x04\x03\x05\x03\x05\x03\x05\x05\x050" +
    "\n\x05\x03\x06\x03\x06\x03\x06\x02\x02\x02\x07\x02\x02\x04\x02\x06\x02" +
    "\b\x02\n\x02\x02\x02\x025\x02\x0F\x03\x02\x02\x02\x04\x1D\x03\x02\x02" +
    "\x02\x06%\x03\x02\x02\x02\b/\x03\x02\x02\x02\n1\x03\x02\x02\x02\f\x0E" +
    "\x07\f\x02\x02\r\f\x03\x02\x02\x02\x0E\x11\x03\x02\x02\x02\x0F\r\x03\x02" +
    "\x02\x02\x0F\x10\x03\x02\x02\x02\x10\x15\x03\x02\x02\x02\x11\x0F\x03\x02" +
    "\x02\x02\x12\x14\x05\x04\x03\x02\x13\x12\x03\x02\x02\x02\x14\x17\x03\x02" +
    "\x02\x02\x15\x13\x03\x02\x02\x02\x15\x16\x03\x02\x02\x02\x16\x18\x03\x02" +
    "\x02\x02\x17\x15\x03\x02\x02\x02\x18\x19\x07\x02\x02\x03\x19\x03\x03\x02" +
    "\x02\x02\x1A\x1E\x05\x06\x04\x02\x1B\x1E\x05\b\x05\x02\x1C\x1E\x05\n\x06" +
    "\x02\x1D\x1A\x03\x02\x02\x02\x1D\x1B\x03\x02\x02\x02\x1D\x1C\x03\x02\x02" +
    "\x02\x1E\"\x03\x02\x02\x02\x1F!\x07\f\x02\x02 \x1F\x03\x02\x02\x02!$\x03" +
    "\x02\x02\x02\" \x03\x02\x02\x02\"#\x03\x02\x02\x02#\x05\x03\x02\x02\x02" +
    "$\"\x03\x02\x02\x02%&\x07\x04\x02\x02&\'\x07\n\x02\x02\'(\x07\x03\x02" +
    "\x02()\x07\n\x02\x02)*\x07\x03\x02\x02*+\x07\x05\x02\x02+\x07\x03\x02" +
    "\x02\x02,0\x07\x06\x02\x02-0\x07\x07\x02\x02.0\x07\b\x02\x02/,\x03\x02" +
    "\x02\x02/-\x03\x02\x02\x02/.\x03\x02\x02\x020\t\x03\x02\x02\x0212\x07" +
    "\t\x02\x022\v\x03\x02\x02\x02\x07\x0F\x15\x1D\"/";
class RobotcommandsContext extends ParserRuleContext_1.ParserRuleContext {
    EOF() { return this.getToken(toyrobotParser.EOF, 0); }
    NEWLINE(i) {
        if (i === undefined) {
            return this.getTokens(toyrobotParser.NEWLINE);
        }
        else {
            return this.getToken(toyrobotParser.NEWLINE, i);
        }
    }
    robotcommand(i) {
        if (i === undefined) {
            return this.getRuleContexts(RobotcommandContext);
        }
        else {
            return this.getRuleContext(i, RobotcommandContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return toyrobotParser.RULE_robotcommands; }
    // @Override
    enterRule(listener) {
        if (listener.enterRobotcommands) {
            listener.enterRobotcommands(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitRobotcommands) {
            listener.exitRobotcommands(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitRobotcommands) {
            return visitor.visitRobotcommands(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.RobotcommandsContext = RobotcommandsContext;
class RobotcommandContext extends ParserRuleContext_1.ParserRuleContext {
    placement() {
        return this.tryGetRuleContext(0, PlacementContext);
    }
    movement() {
        return this.tryGetRuleContext(0, MovementContext);
    }
    report() {
        return this.tryGetRuleContext(0, ReportContext);
    }
    NEWLINE(i) {
        if (i === undefined) {
            return this.getTokens(toyrobotParser.NEWLINE);
        }
        else {
            return this.getToken(toyrobotParser.NEWLINE, i);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return toyrobotParser.RULE_robotcommand; }
    // @Override
    enterRule(listener) {
        if (listener.enterRobotcommand) {
            listener.enterRobotcommand(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitRobotcommand) {
            listener.exitRobotcommand(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitRobotcommand) {
            return visitor.visitRobotcommand(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.RobotcommandContext = RobotcommandContext;
class PlacementContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    PLACE() { return this.getToken(toyrobotParser.PLACE, 0); }
    NUMBER(i) {
        if (i === undefined) {
            return this.getTokens(toyrobotParser.NUMBER);
        }
        else {
            return this.getToken(toyrobotParser.NUMBER, i);
        }
    }
    DIRECTION() { return this.getToken(toyrobotParser.DIRECTION, 0); }
    // @Override
    get ruleIndex() { return toyrobotParser.RULE_placement; }
    // @Override
    enterRule(listener) {
        if (listener.enterPlacement) {
            listener.enterPlacement(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitPlacement) {
            listener.exitPlacement(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitPlacement) {
            return visitor.visitPlacement(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.PlacementContext = PlacementContext;
class MovementContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return toyrobotParser.RULE_movement; }
    copyFrom(ctx) {
        super.copyFrom(ctx);
    }
}
exports.MovementContext = MovementContext;
class MoveForwardContext extends MovementContext {
    MOVE() { return this.getToken(toyrobotParser.MOVE, 0); }
    constructor(ctx) {
        super(ctx.parent, ctx.invokingState);
        this.copyFrom(ctx);
    }
    // @Override
    enterRule(listener) {
        if (listener.enterMoveForward) {
            listener.enterMoveForward(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitMoveForward) {
            listener.exitMoveForward(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitMoveForward) {
            return visitor.visitMoveForward(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.MoveForwardContext = MoveForwardContext;
class RotateLeftContext extends MovementContext {
    LEFT() { return this.getToken(toyrobotParser.LEFT, 0); }
    constructor(ctx) {
        super(ctx.parent, ctx.invokingState);
        this.copyFrom(ctx);
    }
    // @Override
    enterRule(listener) {
        if (listener.enterRotateLeft) {
            listener.enterRotateLeft(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitRotateLeft) {
            listener.exitRotateLeft(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitRotateLeft) {
            return visitor.visitRotateLeft(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.RotateLeftContext = RotateLeftContext;
class RotateRightContext extends MovementContext {
    RIGHT() { return this.getToken(toyrobotParser.RIGHT, 0); }
    constructor(ctx) {
        super(ctx.parent, ctx.invokingState);
        this.copyFrom(ctx);
    }
    // @Override
    enterRule(listener) {
        if (listener.enterRotateRight) {
            listener.enterRotateRight(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitRotateRight) {
            listener.exitRotateRight(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitRotateRight) {
            return visitor.visitRotateRight(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.RotateRightContext = RotateRightContext;
class ReportContext extends ParserRuleContext_1.ParserRuleContext {
    REPORT() { return this.getToken(toyrobotParser.REPORT, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return toyrobotParser.RULE_report; }
    // @Override
    enterRule(listener) {
        if (listener.enterReport) {
            listener.enterReport(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitReport) {
            listener.exitReport(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitReport) {
            return visitor.visitReport(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ReportContext = ReportContext;
//# sourceMappingURL=toyrobotParser.js.map