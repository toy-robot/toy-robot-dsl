import { ATN } from "antlr4ts/atn/ATN";
import { Parser } from "antlr4ts/Parser";
import { ParserRuleContext } from "antlr4ts/ParserRuleContext";
import { TerminalNode } from "antlr4ts/tree/TerminalNode";
import { Token } from "antlr4ts/Token";
import { TokenStream } from "antlr4ts/TokenStream";
import { Vocabulary } from "antlr4ts/Vocabulary";
import { toyrobotListener } from "./toyrobotListener";
import { toyrobotVisitor } from "./toyrobotVisitor";
export declare class toyrobotParser extends Parser {
    static readonly T__0 = 1;
    static readonly PLACE = 2;
    static readonly DIRECTION = 3;
    static readonly MOVE = 4;
    static readonly LEFT = 5;
    static readonly RIGHT = 6;
    static readonly REPORT = 7;
    static readonly NUMBER = 8;
    static readonly WHITESPACE = 9;
    static readonly NEWLINE = 10;
    static readonly RULE_robotcommands = 0;
    static readonly RULE_robotcommand = 1;
    static readonly RULE_placement = 2;
    static readonly RULE_movement = 3;
    static readonly RULE_report = 4;
    static readonly ruleNames: string[];
    private static readonly _LITERAL_NAMES;
    private static readonly _SYMBOLIC_NAMES;
    static readonly VOCABULARY: Vocabulary;
    get vocabulary(): Vocabulary;
    get grammarFileName(): string;
    get ruleNames(): string[];
    get serializedATN(): string;
    constructor(input: TokenStream);
    robotcommands(): RobotcommandsContext;
    robotcommand(): RobotcommandContext;
    placement(): PlacementContext;
    movement(): MovementContext;
    report(): ReportContext;
    static readonly _serializedATN: string;
    static __ATN: ATN;
    static get _ATN(): ATN;
}
export declare class RobotcommandsContext extends ParserRuleContext {
    EOF(): TerminalNode;
    NEWLINE(): TerminalNode[];
    NEWLINE(i: number): TerminalNode;
    robotcommand(): RobotcommandContext[];
    robotcommand(i: number): RobotcommandContext;
    constructor(parent: ParserRuleContext | undefined, invokingState: number);
    get ruleIndex(): number;
    enterRule(listener: toyrobotListener): void;
    exitRule(listener: toyrobotListener): void;
    accept<Result>(visitor: toyrobotVisitor<Result>): Result;
}
export declare class RobotcommandContext extends ParserRuleContext {
    placement(): PlacementContext | undefined;
    movement(): MovementContext | undefined;
    report(): ReportContext | undefined;
    NEWLINE(): TerminalNode[];
    NEWLINE(i: number): TerminalNode;
    constructor(parent: ParserRuleContext | undefined, invokingState: number);
    get ruleIndex(): number;
    enterRule(listener: toyrobotListener): void;
    exitRule(listener: toyrobotListener): void;
    accept<Result>(visitor: toyrobotVisitor<Result>): Result;
}
export declare class PlacementContext extends ParserRuleContext {
    _x: Token;
    _y: Token;
    _d: Token;
    PLACE(): TerminalNode;
    NUMBER(): TerminalNode[];
    NUMBER(i: number): TerminalNode;
    DIRECTION(): TerminalNode;
    constructor(parent: ParserRuleContext | undefined, invokingState: number);
    get ruleIndex(): number;
    enterRule(listener: toyrobotListener): void;
    exitRule(listener: toyrobotListener): void;
    accept<Result>(visitor: toyrobotVisitor<Result>): Result;
}
export declare class MovementContext extends ParserRuleContext {
    constructor(parent: ParserRuleContext | undefined, invokingState: number);
    get ruleIndex(): number;
    copyFrom(ctx: MovementContext): void;
}
export declare class MoveForwardContext extends MovementContext {
    MOVE(): TerminalNode;
    constructor(ctx: MovementContext);
    enterRule(listener: toyrobotListener): void;
    exitRule(listener: toyrobotListener): void;
    accept<Result>(visitor: toyrobotVisitor<Result>): Result;
}
export declare class RotateLeftContext extends MovementContext {
    LEFT(): TerminalNode;
    constructor(ctx: MovementContext);
    enterRule(listener: toyrobotListener): void;
    exitRule(listener: toyrobotListener): void;
    accept<Result>(visitor: toyrobotVisitor<Result>): Result;
}
export declare class RotateRightContext extends MovementContext {
    RIGHT(): TerminalNode;
    constructor(ctx: MovementContext);
    enterRule(listener: toyrobotListener): void;
    exitRule(listener: toyrobotListener): void;
    accept<Result>(visitor: toyrobotVisitor<Result>): Result;
}
export declare class ReportContext extends ParserRuleContext {
    REPORT(): TerminalNode;
    constructor(parent: ParserRuleContext | undefined, invokingState: number);
    get ruleIndex(): number;
    enterRule(listener: toyrobotListener): void;
    exitRule(listener: toyrobotListener): void;
    accept<Result>(visitor: toyrobotVisitor<Result>): Result;
}
