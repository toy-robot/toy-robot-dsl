import { ParseTreeListener } from "antlr4ts/tree/ParseTreeListener";
import { MoveForwardContext } from "./toyrobotParser";
import { RotateLeftContext } from "./toyrobotParser";
import { RotateRightContext } from "./toyrobotParser";
import { RobotcommandsContext } from "./toyrobotParser";
import { RobotcommandContext } from "./toyrobotParser";
import { PlacementContext } from "./toyrobotParser";
import { MovementContext } from "./toyrobotParser";
import { ReportContext } from "./toyrobotParser";
/**
 * This interface defines a complete listener for a parse tree produced by
 * `toyrobotParser`.
 */
export interface toyrobotListener extends ParseTreeListener {
    /**
     * Enter a parse tree produced by the `MoveForward`
     * labeled alternative in `toyrobotParser.movement`.
     * @param ctx the parse tree
     */
    enterMoveForward?: (ctx: MoveForwardContext) => void;
    /**
     * Exit a parse tree produced by the `MoveForward`
     * labeled alternative in `toyrobotParser.movement`.
     * @param ctx the parse tree
     */
    exitMoveForward?: (ctx: MoveForwardContext) => void;
    /**
     * Enter a parse tree produced by the `RotateLeft`
     * labeled alternative in `toyrobotParser.movement`.
     * @param ctx the parse tree
     */
    enterRotateLeft?: (ctx: RotateLeftContext) => void;
    /**
     * Exit a parse tree produced by the `RotateLeft`
     * labeled alternative in `toyrobotParser.movement`.
     * @param ctx the parse tree
     */
    exitRotateLeft?: (ctx: RotateLeftContext) => void;
    /**
     * Enter a parse tree produced by the `RotateRight`
     * labeled alternative in `toyrobotParser.movement`.
     * @param ctx the parse tree
     */
    enterRotateRight?: (ctx: RotateRightContext) => void;
    /**
     * Exit a parse tree produced by the `RotateRight`
     * labeled alternative in `toyrobotParser.movement`.
     * @param ctx the parse tree
     */
    exitRotateRight?: (ctx: RotateRightContext) => void;
    /**
     * Enter a parse tree produced by `toyrobotParser.robotcommands`.
     * @param ctx the parse tree
     */
    enterRobotcommands?: (ctx: RobotcommandsContext) => void;
    /**
     * Exit a parse tree produced by `toyrobotParser.robotcommands`.
     * @param ctx the parse tree
     */
    exitRobotcommands?: (ctx: RobotcommandsContext) => void;
    /**
     * Enter a parse tree produced by `toyrobotParser.robotcommand`.
     * @param ctx the parse tree
     */
    enterRobotcommand?: (ctx: RobotcommandContext) => void;
    /**
     * Exit a parse tree produced by `toyrobotParser.robotcommand`.
     * @param ctx the parse tree
     */
    exitRobotcommand?: (ctx: RobotcommandContext) => void;
    /**
     * Enter a parse tree produced by `toyrobotParser.placement`.
     * @param ctx the parse tree
     */
    enterPlacement?: (ctx: PlacementContext) => void;
    /**
     * Exit a parse tree produced by `toyrobotParser.placement`.
     * @param ctx the parse tree
     */
    exitPlacement?: (ctx: PlacementContext) => void;
    /**
     * Enter a parse tree produced by `toyrobotParser.movement`.
     * @param ctx the parse tree
     */
    enterMovement?: (ctx: MovementContext) => void;
    /**
     * Exit a parse tree produced by `toyrobotParser.movement`.
     * @param ctx the parse tree
     */
    exitMovement?: (ctx: MovementContext) => void;
    /**
     * Enter a parse tree produced by `toyrobotParser.report`.
     * @param ctx the parse tree
     */
    enterReport?: (ctx: ReportContext) => void;
    /**
     * Exit a parse tree produced by `toyrobotParser.report`.
     * @param ctx the parse tree
     */
    exitReport?: (ctx: ReportContext) => void;
}
