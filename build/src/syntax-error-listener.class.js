"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * A syntax error listener for our toy robot.
 */
class SyntaxErrorListener {
    syntaxError(_recogniser, _token, line, column, message
    // _e: unknown
    ) {
        const errorMessage = `${line}:${column + 1} ${message}`;
        const error = new Error(errorMessage);
        throw error;
    }
}
exports.SyntaxErrorListener = SyntaxErrorListener;
//# sourceMappingURL=syntax-error-listener.class.js.map