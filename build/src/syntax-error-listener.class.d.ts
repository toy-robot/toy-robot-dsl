import { ANTLRErrorListener } from 'antlr4ts';
/**
 * A syntax error listener for our toy robot.
 */
export declare class SyntaxErrorListener implements ANTLRErrorListener<unknown> {
    syntaxError(_recogniser: unknown, _token: unknown, line: number, column: number, message: string): void;
}
