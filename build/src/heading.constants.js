"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 *  Define headings starting at NORTH and going clockwise
 * */
exports.HeadingIndex = ['NORTH', 'EAST', 'SOUTH', 'WEST'];
/**
 * Keep movement translations as a simple matrix, no need for fancy arithmetic
 */
exports.HeadingMap = new Map([
    ['NORTH', { x: 0, y: 1 }],
    ['EAST', { x: 1, y: 0 }],
    ['SOUTH', { x: 0, y: -1 }],
    ['WEST', { x: -1, y: 0 }],
]);
//# sourceMappingURL=heading.constants.js.map