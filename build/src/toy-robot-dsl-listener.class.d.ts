import { toyrobotListener } from './grammar/toyrobotListener';
import { MoveForwardContext, PlacementContext, ReportContext, RotateLeftContext, RotateRightContext } from './grammar/toyrobotParser';
import { Transform } from './transform.interface';
import { Observable, Subscriber } from 'rxjs';
import { ToyRobotReport } from './toy-robot-report.interface';
/**
 * The Toy Robot DSL Listener processes the toy robot program.
 */
export declare class ToyRobotDSLListener implements toyrobotListener {
    protected gridSize: Transform;
    /**
     * The position of the robot or undefined if it hasn't yet been placed.
     */
    protected position?: Transform;
    /**
     * The heading index of the robot or undefined if it hasn't yet been placed.
     */
    protected heading?: number;
    protected subscriber?: Subscriber<unknown>;
    /**
     * Initialise the context of the toy robot.
     * @param gridSize Initialised the grid size. Default is 5x5
     */
    constructor(gridSize?: Transform);
    /**
     * Get the current heading as a direction, i.e. NORTH, SOUTH, EAST, WEST
     */
    get Heading(): string | undefined;
    /**
     * Get the current heading as an index of possible directions
     */
    get HeadingIndex(): number | undefined;
    /**
     * Get the current position
     */
    get Position(): Transform | undefined;
    /**
     * Get the current size of the grid
     */
    get GridSize(): Transform;
    /**
     * Tests if a position is within the bounds of the grid
     * @param position A position transform to test
     */
    isInBounds(position: Transform): boolean;
    /**
     * Executes the program for this toy robot's context
     * @param program The program to execute
     */
    execute(program: string): import("./grammar/toyrobotParser").RobotcommandsContext;
    exitRobotcommands(): void;
    enterPlacement(ctx: PlacementContext): void;
    /**
     * Create an rxjs observer that can be subscribed to for monitoring
     * output from the REPORT command.
     */
    reporter(): Observable<ToyRobotReport>;
    enterReport(ctx: ReportContext): void;
    enterMoveForward(ctx: MoveForwardContext): void;
    enterRotateLeft(ctx: RotateLeftContext): void;
    enterRotateRight(ctx: RotateRightContext): void;
}
