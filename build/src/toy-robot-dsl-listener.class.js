"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const toyrobotParser_1 = require("./grammar/toyrobotParser");
const DebugFactory = require("debug");
const heading_constants_1 = require("./heading.constants");
const CharStreams_1 = require("antlr4ts/CharStreams");
const CommonTokenStream_1 = require("antlr4ts/CommonTokenStream");
const toyrobotLexer_1 = require("./grammar/toyrobotLexer");
const ParseTreeWalker_1 = require("antlr4ts/tree/ParseTreeWalker");
const syntax_error_listener_class_1 = require("./syntax-error-listener.class");
const rxjs_1 = require("rxjs");
const debug = DebugFactory('toyrobot:dsl');
/**
 * The Toy Robot DSL Listener processes the toy robot program.
 */
class ToyRobotDSLListener {
    /**
     * Initialise the context of the toy robot.
     * @param gridSize Initialised the grid size. Default is 5x5
     */
    constructor(gridSize = { x: 5, y: 5 }) {
        this.gridSize = gridSize;
    }
    /**
     * Get the current heading as a direction, i.e. NORTH, SOUTH, EAST, WEST
     */
    get Heading() {
        if (this.heading === undefined) {
            debug('[Heading] Valid robot placement is yet to been made.');
            return undefined;
        }
        const theHeading = heading_constants_1.HeadingIndex[this.heading];
        debug(`[Heading] ${theHeading} (${this.heading})`);
        return theHeading;
    }
    /**
     * Get the current heading as an index of possible directions
     */
    get HeadingIndex() {
        return this.heading;
    }
    /**
     * Get the current position
     */
    get Position() {
        return this.position;
    }
    /**
     * Get the current size of the grid
     */
    get GridSize() {
        return this.gridSize;
    }
    /**
     * Tests if a position is within the bounds of the grid
     * @param position A position transform to test
     */
    isInBounds(position) {
        const isBounded = position.x >= 0 &&
            position.y >= 0 &&
            position.x < this.gridSize.x &&
            position.y < this.gridSize.y;
        return isBounded;
    }
    /**
     * Executes the program for this toy robot's context
     * @param program The program to execute
     */
    execute(program) {
        const syntaxErrorListener = new syntax_error_listener_class_1.SyntaxErrorListener();
        const inputStream = CharStreams_1.CharStreams.fromString(program);
        const lexer = new toyrobotLexer_1.toyrobotLexer(inputStream);
        lexer.removeErrorListeners();
        lexer.addErrorListener(syntaxErrorListener);
        const tokenStream = new CommonTokenStream_1.CommonTokenStream(lexer);
        const parser = new toyrobotParser_1.toyrobotParser(tokenStream);
        parser.removeErrorListeners();
        parser.addErrorListener(syntaxErrorListener);
        const tree = parser.robotcommands();
        ParseTreeWalker_1.ParseTreeWalker.DEFAULT.walk(this, tree);
        return tree;
    }
    exitRobotcommands() {
        var _a;
        (_a = this.subscriber) === null || _a === void 0 ? void 0 : _a.complete();
    }
    enterPlacement(ctx) {
        // Although these next 3 ctx properties are optional, they will always be set because of the structure of our grammar
        // Further, they will always be of the right type, the grammar rules act as a validator
        const x = +ctx._x.text;
        const y = +ctx._y.text;
        const headingName = ctx._d.text;
        const heading = heading_constants_1.HeadingIndex.indexOf(headingName);
        const position = { x, y };
        if (this.isInBounds(position)) {
            debug(`[PLACE] x:${x}, y: ${y}, heading: ${headingName} (${heading})}`);
            this.position = position;
            this.heading = heading;
        }
    }
    /**
     * Create an rxjs observer that can be subscribed to for monitoring
     * output from the REPORT command.
     */
    reporter() {
        return new rxjs_1.Observable(subscriber => {
            this.subscriber = subscriber;
        });
    }
    enterReport(ctx) {
        var _a, _b;
        const report = {
            headingIndex: (_a = this.HeadingIndex) !== null && _a !== void 0 ? _a : undefined,
            headingName: this.Heading,
            position: this.Position,
        };
        debug(`[REPORT] Report: ${JSON.stringify(report)}`);
        (_b = this.subscriber) === null || _b === void 0 ? void 0 : _b.next(report);
    }
    enterMoveForward(ctx) {
        if (!this.position) {
            debug('[MOVE] Robot can not move, it needs to be positioned first.');
            return;
        }
        const heading = heading_constants_1.HeadingIndex[this.heading];
        const movementTransform = heading_constants_1.HeadingMap.get(heading);
        const nextPosition = {
            x: this.position.x + movementTransform.x,
            y: this.position.y + movementTransform.y,
        };
        if (!this.isInBounds(nextPosition)) {
            debug(`[MOVE] Can not move off grid: ${this.position} => ${nextPosition}, ${this.Heading} (${this.heading}) ]`);
            return;
        }
        this.position = nextPosition;
        debug(`[MOVE] New position: ${this.position}, ${this.Heading} (${this.heading}) ]`);
    }
    enterRotateLeft(ctx) {
        if (!this.position) {
            debug('[LEFT] Robot can not rotate, it needs to be positioned first.');
            return;
        }
        this.heading -= this.heading === 0 ? -(heading_constants_1.HeadingIndex.length - 1) : 1;
        debug(`[LEFT] Now facing ${this.Heading} (index: ${this.heading})`);
    }
    enterRotateRight(ctx) {
        if (!this.position) {
            debug('[RIGHT] Robot can not rotate, it needs to be positioned first.');
            return;
        }
        this.heading = (this.heading + 1) % heading_constants_1.HeadingIndex.length;
        debug(`[LEFT] Now facing ${this.Heading} (index: ${this.heading})`);
    }
}
exports.ToyRobotDSLListener = ToyRobotDSLListener;
//# sourceMappingURL=toy-robot-dsl-listener.class.js.map